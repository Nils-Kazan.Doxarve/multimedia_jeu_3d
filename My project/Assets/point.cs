using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    public Transform next_target;  // La cible actuelle
    public Transform[] targets;    // Liste des cibles
    public string Destination_dechet; // Destination des déchets

    public Transform[] Skin;       // Liste des skins associés aux cibles
    int index = 0;                 // Index de la cible actuelle

    // Start est appelé avant la première frame update
    void Start()
    {
        Skin[1].gameObject.SetActive(false);
        transform.localScale = new Vector3(4, 4, 4);
        Debug.Log("Point script started. Initial target: " + (next_target != null ? next_target.name : "None"));
    }

    // Update est appelé une fois par frame
    void Update()
    {
        // Logique potentielle
    }

    

    // Méthode pour changer la cible suivante
    public void ChangeNextTarget()
    {
        if (targets.Length == 0) return;

        // Désactiver le skin actuel
        Skin[index].gameObject.SetActive(false);

        // Calculer le nouvel index
        index = (index + 1) % targets.Length;

        // Activer le nouveau skin
        Skin[index].gameObject.SetActive(true);

        // Mettre à jour la cible suivante
        next_target = targets[index];

        Debug.Log("Target changed to: " + next_target.name);
    }

}
