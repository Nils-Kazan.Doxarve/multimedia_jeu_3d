using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manette : MonoBehaviour
{
    public Point point_de_direction; 


    // Start est appelé avant la première frame update
    void Start()
    {
   
    }

    // Update est appelé une fois par frame
    void Update()
    {
        // Logique potentielle
    }

    private void OnMouseOver()
    {
        transform.localScale = new Vector3(60, 60, 60);
    }

    private void OnMouseExit()
    {
        transform.localScale = new Vector3(40f, 40f, 40f);
    }

    private void OnMouseDown()
    {
        if (point_de_direction != null)
        {
            Debug.Log("Manette clicked. Changing target...");
            point_de_direction.ChangeNextTarget();
        
            
        }
        else
        {
            Debug.LogWarning("PointDeDirection reference not set in Manette script.");
        }
    }

    

   
}
 