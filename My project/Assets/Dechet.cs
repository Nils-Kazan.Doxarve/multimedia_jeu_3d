using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Dechet : MonoBehaviour
{
    public Transform target; // Cible actuelle
    public float speed; // Vitesse de mouvement
    public string type_Dechet; // Type de déchet
    public Spawner Spawnpoint; // Spawner
    public bool Excepted; // Exception pour le premier déchet
    

    void Update()
    {
        // Déplacer l'objet vers la cible
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

            // Si l'objet atteint la cible et qu'il y a une nouvelle cible, changer de cible
            if (transform.position == target.position)
            {
                Point targetPoint = target.gameObject.GetComponent<Point>();
                if (targetPoint != null && targetPoint.next_target != null) 
                {
                    target = targetPoint.next_target;
                }
                else
                {   
                    
                    string destination = targetPoint.Destination_dechet;
                    if (Excepted == false)
                    {


                        if (destination == type_Dechet)
                        {
                            Debug.Log("Action réussie");
                            Spawnpoint.objectSpeed = Spawnpoint.objectSpeed + 0.2f;
                            Spawnpoint.spawnInterval = 12f / Spawnpoint.objectSpeed;

                        }
                        else
                        {
                            Debug.Log("Mauvaise poubelle");
                            Spawnpoint.objectSpeed = Math.Max(1.2f, Spawnpoint.objectSpeed - 1f);
                            Spawnpoint.spawnInterval = 12f / Spawnpoint.objectSpeed;

                        }
                        Destroy(gameObject);
                    }

                }
                
            }
        }
    }

    

    // Méthode pour initialiser l'objet avec une cible spécifique
    public void Initialize(Transform newTarget, float newSpeed, string newTypeDechet)
    {
        target = newTarget;
        speed = newSpeed;
        type_Dechet = newTypeDechet;
        Excepted = false;
        
    }

}
