using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    public Transform spawnPoint; 
    public float spawnInterval = 10f;
    public Transform initialTarget;
    public float objectSpeed = 1.2f;
    public string[] typeDechets = {"pet","papier","non-recyclable","organique"};
    public Material materialPet;
    public Material materialNonRecyclable;
    public Material materialOrganic;
    public Material materialPapier;

    // Start est appelé avant la première frame update
    void Start()
    {
        
        StartCoroutine(SpawnRoutine());
    }

    // Routine de spawn
    IEnumerator SpawnRoutine()
    {
        while (true)
        {
        
            yield return new WaitForSeconds(spawnInterval);
            SpawnObject();
        }
    }


    void SpawnObject()
    {
        
        GameObject newObject = Instantiate(objectToSpawn, spawnPoint.position, spawnPoint.rotation);
        
       
        Dechet dechetScript = newObject.GetComponent<Dechet>();
        MeshRenderer meshRenderer = newObject.GetComponent<MeshRenderer>();
        if (dechetScript != null)
        {
            int randomNumber = Random.Range(0,4);
            string TypeDechet = typeDechets[randomNumber];
            dechetScript.Initialize(initialTarget, objectSpeed, TypeDechet);
            Debug.Log("spawn de " + TypeDechet);
            if (meshRenderer != null && TypeDechet == "pet" && materialPet != null)
            {
                meshRenderer.material = materialPet;
            }
            if (meshRenderer != null && TypeDechet == "organique" && materialOrganic != null)
            {
                meshRenderer.material = materialOrganic;
            }
            if (meshRenderer != null && TypeDechet == "papier" && materialPapier != null)
            {
                meshRenderer.material = materialPapier;
            }
            if (meshRenderer != null && TypeDechet == "non-recyclable" && materialNonRecyclable != null)
            {
                meshRenderer.material = materialNonRecyclable;
            }
        }
    }
}
